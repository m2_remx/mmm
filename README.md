# Please read this README #

This repo contains the android application project **MagicRecipe** designed for the GLA-MMM project 2016.


## Gradle Configurations ##

* ###compileSdkVersion 23###
* ###buildToolsVersion "23.0.2"###

## Gradle Dependencies ##
### Google Services ###

This application uses GoogleServices in order to get access to Google Map APIs.
Please, make sure to handle all the access to Google Services functionalities before running the App (For Android Virtual Devices (AVD), make sure to install **Google Apis** using SDK Manager.

* compile 'com.firebase:firebase-client-android:2.5.2'
* ###compile 'com.google.android.gms:play-services-maps:8.4.0'###
* compile 'com.android.support:design:23.2.1'
* compile 'com.android.support:support-v4:23.2.1'


# Authors #
* ##Audrey Charbonneau-Pin##
* ##Mamadou Dian Diallo##
* ##Mónica Fernández##
* ##Mickaël Callimoutou##