package fr.istic.mmm.gla.projet.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import fr.istic.mmm.gla.projet.R;
import fr.istic.mmm.gla.projet.domain.Recipe;
import fr.istic.mmm.gla.projet.domain.RecipeParcelable;


/**
 * Created by MagicTeam on 21/03/16.
 */
public class ListRecipeFragment extends Fragment implements AdapterView.OnItemClickListener {


    /**  List View **/
    private ListView listView;
    /**  List Recipies **/
    List<Recipe> recipes;

    /**  Activity that contains this Fragment **/
    private OnListRecipeFragmentInteractionListener mListener;

    /**
     * Default Constructor
     */
    public ListRecipeFragment() {
        recipes = new ArrayList<>();
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment ListRecipeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ListRecipeFragment newInstance() {
        ListRecipeFragment fragment = new ListRecipeFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }



    /**     *     Fragment Life Cycle    *    **/

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_list_recipe, container, false);

        listView = (ListView) view.findViewById(R.id.listrecipies);
        listView.setOnItemClickListener(this);

        initFireBase();

        return view;
    }

    /**
     * Init the connection to FireBase database
     * Add a ValueEventListener
     */
    private void initFireBase(){
        Firebase myFirebaseRef;
        Firebase.setAndroidContext(getActivity());
        /**     -     -     -     FIREBASE     -     -     -     **/
        myFirebaseRef = new Firebase("https://magicrecipe.firebaseio.com/recipies");
        myFirebaseRef.addValueEventListener(new thisFireBaseListener());
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListRecipeFragmentInteractionListener) {
            mListener = (OnListRecipeFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        /* this is bad practice... */
        listView = null;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        mListener.newRecipeIsSelected(new RecipeParcelable(recipes.get(position)));
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     */
    public interface OnListRecipeFragmentInteractionListener {
        void newRecipeIsSelected(RecipeParcelable recipe);
    }

    /**
     *
     */
    public void refreshListRecipies() {
        if(getActivity() != null) {
            if(recipes != null) {
                if(recipes.size() != 0) {
                    String[] titles = new String[recipes.size()];
                    int i=0;
                    for(Recipe recipe : recipes) {
                        titles[i++]= recipe.getTitle();
                    }

                    System.out.println(" xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx ");
                    System.out.println("getActivity : " + getActivity());
                    System.out.println("titles : " + titles);
                    System.out.println(" xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx ");
                    ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, titles);
                    listView.setAdapter(adapter);

                }
            }
        }
    }

    /**
     *
     */
    public class thisFireBaseListener implements ValueEventListener {

        public thisFireBaseListener() {
        }

        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            recipes = new ArrayList<>();
            for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                Recipe recipe = postSnapshot.getValue(Recipe.class);
                recipes.add(recipe);
            }
            refreshListRecipies();
        }

        @Override
        public void onCancelled(FirebaseError firebaseError) {

        }
    }

}




