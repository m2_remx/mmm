package fr.istic.mmm.gla.projet.domain;

/**
 * Created by MagicTeam on 21/03/16.
 */
public class Recipe {

    /**     -       Attributes        -      **/

    private String _id;
    /** Name or Title of the recipe **/
    private String title;
    /** Author of the recipe **/
    private String author;
    /** Size of the meal. Number of people **/
    private String size;
    /** Content of the recipe **/
    private String description;
    /** part of total duration: minutes needed to execute the recipe **/
    private String durationmin;
    /** part of total duration: hours needed to execute the recipe **/
    private String durationhour;
    /** latitude of the recipe **/
    private String latitude;
    /** longitude of the recipe **/
    private String longitude;



    /**     -       Constructors        -      **/

    /**
     *  Default Constructor
     */
    public Recipe(){}


    public Recipe(String _id, String title, String author, String size, String description, String durationmin, String durationhour, String latitude, String longitude) {
        this._id = _id;
        this.title = title;
        this.author = author;
        this.size = size;
        this.description = description;
        this.durationmin = durationmin;
        this.durationhour = durationhour;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    /**     -       Getter & Setters        -      **/


    public String getTitle() {
        return title;
    }

    public void setTitle(String name) {
        this.title = name;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDurationmin() {
        return durationmin;
    }

    public void setDurationmin(String durationmin) {
        this.durationmin = durationmin;
    }


    public String getDurationhour() {
        return durationhour;
    }

    public void setDurationhour(String durationhour) {
        this.durationhour = durationhour;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    @Override
    public String toString() {
        return "Recipe{" +
                "_id='" + _id + '\'' +
                ", title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", size='" + size + '\'' +
                ", description='" + description + '\'' +
                ", durationmin='" + durationmin + '\'' +
                ", durationhour='" + durationhour + '\'' +
                ", latitude='" + latitude + '\'' +
                ", longitude='" + longitude + '\'' +
                '}';
    }
}
