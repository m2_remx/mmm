package fr.istic.mmm.gla.projet;

import android.content.Intent;
import android.content.res.Configuration;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import fr.istic.mmm.gla.projet.fragment.ListRecipeFragment;
import fr.istic.mmm.gla.projet.fragment.ViewRecipeFragment;
import fr.istic.mmm.gla.projet.domain.RecipeParcelable;

/**
 * Created by MagicTeam on 21/03/16.
 */
public class ViewRecipeActivity extends AppCompatActivity
        implements ViewRecipeFragment.OnFragmentInteractionListener,
        ListRecipeFragment.OnListRecipeFragmentInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_recipe);


        /*   - - - - - - TODO: If Click From Map  - - - -  */
        Intent intent = getIntent();
        Fragment mapFragment;
        RecipeParcelable parcelable = intent.getExtras().getParcelable(ViewRecipeFragment.PARCELABLE);

        if ( parcelable != null ) {
            mapFragment = ViewRecipeFragment.newInstance(parcelable);
            FragmentManager fragmentManager =  getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.container_fragment_viewrecipe, mapFragment).commit();
        }

        /**  Detect if we're on a smart phone or a TABLET  **/
        boolean isLarge = ((getResources().getConfiguration().screenLayout &
                Configuration.SCREENLAYOUT_SIZE_MASK) ==
                Configuration.SCREENLAYOUT_SIZE_LARGE) ||
                ((getResources().getConfiguration().screenLayout &
                        Configuration.SCREENLAYOUT_SIZE_MASK) ==
                        Configuration.SCREENLAYOUT_SIZE_XLARGE);

        if(isLarge){
            Fragment mapFragmentListRecipies = new ListRecipeFragment();
            FragmentManager fragmentManagerListRecipies =  getSupportFragmentManager();
            fragmentManagerListRecipies.beginTransaction().replace(R.id.container_fragment_listrecipes, mapFragmentListRecipies).commit();
        }




    }


    @Override
    public void newRecipeIsSelected(RecipeParcelable recipe) {
        Fragment mapFragment = ViewRecipeFragment.newInstance(recipe);
        FragmentManager fragmentManager =  getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.container_fragment_viewrecipe, mapFragment).commit();
    }
}
