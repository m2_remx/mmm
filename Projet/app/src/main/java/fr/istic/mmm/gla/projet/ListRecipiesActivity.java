package fr.istic.mmm.gla.projet;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import fr.istic.mmm.gla.projet.fragment.ListRecipeFragment;
import fr.istic.mmm.gla.projet.domain.RecipeParcelable;

/**
 * Created by MagicTeam on 21/03/16.
 */
public class ListRecipiesActivity extends AppCompatActivity
        implements ListRecipeFragment.OnListRecipeFragmentInteractionListener {


    private static final String PARCELABLE="parcelable";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_recipies);



        /**  Add the First Fragment of the App : The Map  **/
        Fragment mapFragment = new ListRecipeFragment();
        FragmentManager fragmentManager =  getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.container_fragmentactivity_listrecipies, mapFragment).commit();

    }



    @Override
    public void newRecipeIsSelected(RecipeParcelable recipe) {
        Intent intent = new Intent(this, ViewRecipeActivity.class);
        intent.putExtra(PARCELABLE, recipe);
        startActivity(intent);
    }
}
