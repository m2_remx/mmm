package fr.istic.mmm.gla.projet.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by MagicTeam on 21/03/16.
 */
public class RecipeParcelable implements Parcelable {

    /**  -   Attributes  -  **/
    String _id;
    String title;
    String size;
    String description;
    String latitude;
    String longitude;
    String durationhour;
    String durationmin;
    String author;


    /**  -   Standard Constructor  -  **/
    public RecipeParcelable(String _id, String title,
                            String size,
                            String description,
                            String latitude,
                            String longitude,
                            String durationhour,
                            String durationmin,
                            String author){
        this._id = _id;
        this.title = title;
        this.size = size;
        this.description = description;
        this.latitude = latitude;
        this.longitude = longitude;
        this.durationhour = durationhour;
        this.durationmin = durationmin;
        this.author = author;
    }

    /**  -   Standard Constructor  -  **/
    public RecipeParcelable(Recipe recipe){
        this._id = recipe.get_id();
        this.title = recipe.getTitle();
        this.size = recipe.getSize();
        this.description = recipe.getDescription();
        this.latitude = recipe.getLatitude();
        this.longitude = recipe.getLongitude();
        this.durationhour = recipe.getDurationhour();
        this.durationmin = recipe.getDurationmin();
        this.author = recipe.getAuthor();
    }

    /**  -   Reads In Constructor  -  **/
    protected RecipeParcelable(Parcel in) {
        readFromParcel(in);
    }

    public static final Creator<RecipeParcelable> CREATOR = new Creator<RecipeParcelable>() {
        @Override
        public RecipeParcelable createFromParcel(Parcel in) {
            return new RecipeParcelable(in);
        }

        @Override
        public RecipeParcelable[] newArray(int size) {
            return new RecipeParcelable[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    /**  -   Write To Parcel   -  **/
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(_id);
        dest.writeString(title);
        dest.writeString(size);
        dest.writeString(description);
        dest.writeString(latitude);
        dest.writeString(longitude);
        dest.writeString(durationhour);
        dest.writeString(durationmin);
        dest.writeString(author);
    }

    /**  -   Read From Parcel -  **/
    private void readFromParcel(Parcel in) {
        _id = in.readString();
        title = in.readString();
        size = in.readString();
        description = in.readString();
        latitude = in.readString();
        longitude = in.readString();
        durationhour = in.readString();
        durationmin = in.readString();
        author = in.readString();
    }

    /**  -   Getters   -  **/
    public String getTitle() {
        return title;
    }

    public String getSize() {
        return size;
    }

    public String getDescription() {
        return description;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getDurationhour() {
        return durationhour;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public void setDurationhour(String durationhour) {
        this.durationhour = durationhour;
    }

    public String getDurationmin() {
        return durationmin;
    }

    public void setDurationmin(String durationmin) {
        this.durationmin = durationmin;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }
}