package fr.istic.mmm.gla.projet.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.Query;
import com.firebase.client.ValueEventListener;

import fr.istic.mmm.gla.projet.R;
import fr.istic.mmm.gla.projet.domain.Recipe;
import fr.istic.mmm.gla.projet.domain.RecipeParcelable;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ViewRecipeFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ViewRecipeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ViewRecipeFragment extends Fragment {

    public static final String PARCELABLE = "parcelable";

    private OnFragmentInteractionListener mListener;

    /*  *    Recipe Attributes    *  */
    private TextView text_title;
    private TextView text_description;
    private TextView text_size;
    private TextView text_durationmin;
    private TextView text_durationhour;

    public ViewRecipeFragment() {
        // DON'T DELETE
    }

    /**
     * Return a new Instance of this Fragment with a Recipe
     * @param recipe
     * @return
     */
    public static ViewRecipeFragment newInstance(RecipeParcelable recipe) {
        ViewRecipeFragment fragment = new ViewRecipeFragment();
        Bundle args = new Bundle();
        args.putParcelable(PARCELABLE, recipe);
        fragment.setArguments(args);

        return fragment;
    }


    /**     *     Fragment Life Cycle    *    **/

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_view_recipe, container, false);

        /** Init the View Text Fields **/
        text_title = (TextView) v.findViewById(R.id.viewrecipe_text_title);
        text_description = (TextView) v.findViewById(R.id.viewrecipe_text_description);
        text_size = (TextView) v.findViewById(R.id.viewrecipe_text_size);
        text_durationmin = (TextView) v.findViewById(R.id.viewrecipe_text_duration);

        /** Fill the fields with Value of the Parcelable **/
        RecipeParcelable myRecipeParcelable = getArguments().getParcelable(PARCELABLE);
        if(myRecipeParcelable != null) {
            text_title.setText(myRecipeParcelable.getTitle());
            text_description.setText(myRecipeParcelable.getDescription());
            text_size.setText(myRecipeParcelable.getSize());
            text_durationmin.setText(myRecipeParcelable.getDurationmin());
        }

        return v;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    /**
     * Interface that the Activity that contains this Fragment must implements
     */
    public interface OnFragmentInteractionListener {
    }

}
