package fr.istic.mmm.gla.projet;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.LocationSource;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import fr.istic.mmm.gla.projet.domain.Recipe;
import fr.istic.mmm.gla.projet.fragment.ViewRecipeFragment;
import fr.istic.mmm.gla.projet.notification.PermissionUtils;
import fr.istic.mmm.gla.projet.domain.RecipeParcelable;

/**
 * This shows how to create a simple activity with a map and a marker on the map.
 */
public class MainMagicRecipeActivity extends AppCompatActivity
        implements OnMapReadyCallback,
        OnClickListener,
        GoogleMap.OnMarkerClickListener,
        GoogleMap.OnMyLocationButtonClickListener,
        ActivityCompat.OnRequestPermissionsResultCallback {


    /**
     * FireBaseConnection
     */
    private Firebase myFirebaseRef;

    /**
     * Buttons
     *
     */
    Button buttonAddNewRecipe;
    Button buttonEnterNewZone;
    Button buttonCloseRecipes;

    /**
     * Marker Map
     */
    private HashMap<Marker, Recipe> recipeMarkerMap;


    /** OnLocationChangedListener **/
    private LocationSource.OnLocationChangedListener onLocationChangedListener;


    /**
     * Current Location
     * Use for geo-localization
     */
    Location currentLocation;

    /**
     * Google Map
     */
    private GoogleMap googleMap;

    /**
     * LOCATION REQUEST PERMISSION CODE
     */
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.basic_demo);

        /**     -     -     -     FIREBASE     -     -     -     **/
        Firebase.setAndroidContext(this);
        Firebase.getDefaultConfig().setPersistenceEnabled(true);
        /**     -     -     -     -     -     -     -     -     **/
        SupportMapFragment mapFragment =
                (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        boolean isLarge = ((getResources().getConfiguration().screenLayout &
                Configuration.SCREENLAYOUT_SIZE_MASK) ==
                Configuration.SCREENLAYOUT_SIZE_LARGE) ||
                ((getResources().getConfiguration().screenLayout &
                        Configuration.SCREENLAYOUT_SIZE_MASK) ==
                        Configuration.SCREENLAYOUT_SIZE_XLARGE);

        buttonAddNewRecipe = (Button) findViewById(R.id.button_addnewrecipe);
        buttonAddNewRecipe.setOnClickListener(this);
        if(!isLarge){
            buttonEnterNewZone = (Button) findViewById(R.id.button_viewlistrecipes);
            buttonEnterNewZone.setOnClickListener(this);
        }
        buttonCloseRecipes = (Button) findViewById(R.id.button_closerecipes);
        buttonCloseRecipes.setOnClickListener(this);

        initLocationListener();
    }


    /**
     *
     */
    @Override
    public void onMapReady(GoogleMap map) {
        googleMap = map;
        map.setOnMarkerClickListener(this);
        googleMap.setOnMyLocationButtonClickListener(this);

        enableMyLocation();

        /**     -     -     -     FIREBASE     -     -     -     **/
        myFirebaseRef = new Firebase("https://magicrecipe.firebaseio.com/recipies");
        myFirebaseRef.addValueEventListener(new myFireBaseListener());
        /**     -     -     -     -     -     -     -     -     **/
    }

    /**
     * Enables the My Location layer if the fine location permission has been granted.
     */
    private void enableMyLocation() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission to access the location is missing.
            PermissionUtils.requestPermission(this, LOCATION_PERMISSION_REQUEST_CODE,
                    Manifest.permission.ACCESS_FINE_LOCATION, true);
        }
        else if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission to access the location is missing.
            PermissionUtils.requestPermission(this, LOCATION_PERMISSION_REQUEST_CODE,
                    Manifest.permission.ACCESS_COARSE_LOCATION, true);
        }
        else if (googleMap != null) {
            // Access to the location has been granted to the app.
            googleMap.setMyLocationEnabled(true);
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_addnewrecipe:
                Intent intentAddNewRecipe = new Intent(this, AddRecipeActivity.class);
                startActivity(intentAddNewRecipe);
                break;
            case R.id.button_viewlistrecipes:
                Intent intentListRecipes = new Intent(this, ListRecipiesActivity.class);
                startActivity(intentListRecipes);
                break;
            case R.id.button_closerecipes:
                Toast.makeText(MainMagicRecipeActivity.this, "Not Implemented Yet", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        Recipe recipeSelected = recipeMarkerMap.get(marker);

        Intent intent = new Intent(this, ViewRecipeActivity.class);
        System.out.println(recipeSelected);
        intent.putExtra(ViewRecipeFragment.PARCELABLE, new RecipeParcelable(recipeSelected));
        startActivity(intent);

        return false;
    }

    @Override
    public boolean onMyLocationButtonClick() {
        if(currentLocation != null){
            CameraUpdate update = CameraUpdateFactory.newLatLngZoom(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()),2);
            googleMap.animateCamera(update);
        }
        return false;
    }


    /**
     * Add Markers to the Map
     * @param recipies
     */
    public void addMarks(List<Recipe> recipies) {
        recipeMarkerMap = new HashMap<>();
        googleMap.clear();
        for (Recipe recipe : recipies) {
            Marker marker = googleMap.addMarker(new MarkerOptions().position(new LatLng(new Double(recipe.getLatitude()), new Double(recipe.getLongitude())))
                    .title(recipe.getTitle()));

            recipeMarkerMap.put(marker, recipe);
        }
    }

    /**
     * FireBase Connection and Behaviour
     */
    public class myFireBaseListener implements ValueEventListener {
        public myFireBaseListener() {}

        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            List<Recipe> recipies = new ArrayList<>();
            /* - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
            for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                Recipe recipe = postSnapshot.getValue(Recipe.class);
                /** Set Real ID **/
                recipe.set_id(postSnapshot.getKey());
                recipies.add(recipe);
            }
            addMarks(recipies);
        }

        @Override
        public void onCancelled(FirebaseError firebaseError) {}
    }


    /**
     * OnRequestPermissionsResults
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode != LOCATION_PERMISSION_REQUEST_CODE) {
            return;
        }

        if (PermissionUtils.isPermissionGranted(permissions, grantResults,
                Manifest.permission.ACCESS_FINE_LOCATION)) {
            enableMyLocation();
        }
        else if (PermissionUtils.isPermissionGranted(permissions, grantResults,
                Manifest.permission.ACCESS_COARSE_LOCATION)) {
            enableMyLocation();
        }
    }


    private void initLocationListener() {
        // Acquire a reference to the system Location Manager
        LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        // Define a listener that responds to location updates
        LocationListener locationListener = new LocationListener() {
            public void onLocationChanged(Location location) {
                // Called when a new location is found by the network location provider.
                currentLocation = location;
            }

            public void onStatusChanged(String provider, int status, Bundle extras) {
            }

            public void onProviderEnabled(String provider) {
            }

            public void onProviderDisabled(String provider) {
            }
        };

        // Register the listener with the Location Manager to receive location updates
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            System.out.println("------------BIG ERROR-----------");
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 3000, 0, locationListener);
        //locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
    }

}
