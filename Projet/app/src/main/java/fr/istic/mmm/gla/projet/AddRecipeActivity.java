package fr.istic.mmm.gla.projet;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.firebase.client.Firebase;

import java.util.Date;
import java.util.Random;

import fr.istic.mmm.gla.projet.domain.Recipe;

/**
 * Created by MagicTeam on 21/03/16.
 */
public class AddRecipeActivity extends AppCompatActivity implements View.OnClickListener {

    /**     -       Attributes        -      **/
    private Recipe recipe;

    EditText title;
    EditText description;

    /**
     * FireBaseConnection
     */
    private Firebase myFirebaseRef;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_recipe);


        recipe = new Recipe();

        //** Init GUI Components **/
        initSpinnerSizeOfRecipe();
        initButtonValidNewRecipe();
        initSpinnerDurationOfRecipe();

        title = (EditText) findViewById(R.id.text_title);
        title.requestFocus();

        myFirebaseRef = new Firebase("https://magicrecipe.firebaseio.com/recipies");
    }


    /**
     *
     */
    private void initSpinnerSizeOfRecipe(){

        final int SIZEMAX = 5;
        String[] state= new String[SIZEMAX];
        for(Integer i=1; i < SIZEMAX; i++) {
            state[i-1] ="0"+ i.toString();
        }

        String[] state2= {"0","1","2","3","4","5", "6", "7","8", "9","10"};
        ArrayAdapter<String> adapter_state = new ArrayAdapter<>(this,  android.R.layout.simple_spinner_item, state2);
        //adapter_state.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        Spinner spinnerSizeOfRecipe = (Spinner) findViewById(R.id.spinner_sizeinpeople);
        spinnerSizeOfRecipe.setAdapter(adapter_state);

    }

    /**
     *
     */
    private void initSpinnerDurationOfRecipe(){
        final int HOURMAX = 23;
        final int MINUTEMAX = 59;
        String[] hours= new String[HOURMAX];
        String[] minutes= new String[MINUTEMAX];
        for(Integer i=1; i < HOURMAX; i++) {
            if(i<10) {
                hours[i-1]="0" + i.toString();
            }
            else {
                hours[i - 1] = i.toString();
            }
        }
        for(Integer i=1; i < MINUTEMAX; i++) {
            if(i<10) {
                minutes[i-1]="0" + i.toString();
            }
            else {
                minutes[i - 1] = i.toString();
            }
        }
        ArrayAdapter<String> adapter_h = new ArrayAdapter<String>(this,  android.R.layout.simple_spinner_item, hours);
        Spinner spinnerDurationHour = (Spinner) findViewById(R.id.spinner_durationhour);
        spinnerDurationHour.setAdapter(adapter_h);

        /*
        spinnerDurationHour.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                recipe.setDurationmin(Integer.parseInt(parent.getItemAtPosition(position).toString()));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });*/

        ArrayAdapter<String> adapter_m = new ArrayAdapter<String>(this,  android.R.layout.simple_spinner_item, hours);
        Spinner spinnerDurationMinute = (Spinner) findViewById(R.id.spinner_durationminute);
        spinnerDurationMinute.setAdapter(adapter_m);

        /*
        spinnerDurationMinute.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                recipe.setDurationmin(Integer.parseInt(parent.getItemAtPosition(position).toString()));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });*/



    }

    /**
     *
     */
    private void initButtonValidNewRecipe(){
        Button buttonValidNewRecipe = (Button) findViewById(R.id.button_validnewrecipe);
        buttonValidNewRecipe.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        String id = String.valueOf((new Date()).getTime());
        String title = ((EditText) findViewById(R.id.text_title)).getText().toString();
        String author = "New Author";

        String size =   ((Spinner) findViewById(R.id.spinner_sizeinpeople)).getSelectedItem().toString();
        String description = ((EditText) findViewById(R.id.text_description)).getText().toString();

        String durationmin=  ((Spinner) findViewById(R.id.spinner_durationminute)).getSelectedItem().toString();

        String durationhour=  ((Spinner) findViewById(R.id.spinner_durationhour)).getSelectedItem().toString();


        Random r = new Random();
        String latitud = ((Integer)r.nextInt(90)).toString();
        String longitud = ((Integer)r.nextInt(90)).toString();


        Recipe newRecipe = new Recipe(id,title, author, size, description, durationmin, durationhour, latitud, longitud);
        myFirebaseRef.child(id).setValue(newRecipe);

        finish();
    }
}
